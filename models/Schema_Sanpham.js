 const mongoose = require("mongoose")
 const sanphamSchema = new  mongoose.Schema(
    {
      linkimage: String,
      TenHang: String,
      MaSP: String,
      DanhMuc: String,
      CuaHang: String,
      GiaNhap: Number,
      GiaBan: Number,
      NhaCungCap: String,
      SoLuong: Number,
      account :{
        type :mongoose.Types.ObjectId,     
      }
    },   
  );
const CreateSanpham = mongoose.model("sampham", sanphamSchema);
module.exports = CreateSanpham;
