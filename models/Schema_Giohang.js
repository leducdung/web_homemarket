const mongoose = require("mongoose")
const cardSchema = new  mongoose.Schema({
    soluong: Number,
    tenhang: String,
    tygia: Number,
    tinhtrang: String,
    account: {
      type: mongoose.Types.ObjectId,
      required: true
    }
  });
  const CardSchema = mongoose.model("giohang", cardSchema)
  module.exports= CardSchema;