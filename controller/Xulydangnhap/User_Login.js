  module.exports = function(req, res, next) {
     
    if (req.cookies.email) {
      res.redirect("/trangchu");
    } else res.render("pages/login");
  }