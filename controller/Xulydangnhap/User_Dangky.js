
 const CreateAccount = require("../../models/Schema_account")
 const bcrypt = require("bcrypt");
 const saltRounds = 10;
 module.exports =  function(req, res, next) {
  var { name, email, password } = req.body;
  bcrypt.hash(password, saltRounds, function(err, hash) {
    CreateAccount.create(
      { name, email, password: hash, quyentruycap: "nguoidung" },
      function(err) {
        if (err) {
          console.log(err);
        }
        res.redirect("/login");
      }
    );
  });
}