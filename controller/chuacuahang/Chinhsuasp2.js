const Chinhsuasp2 = require("../../models/Schema_Sanpham")
module.exports = function (req, res, next) {
    const {
      TenHang,
      MaSP,
      DanhMuc,
      CuaHang,
      GiaNhap,
      GiaBan,
      NhaCungCap,
      SoLuong,
    } = req.body;
    CreateSanPham.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      {
        $set: {
          TenHang,
          MaSP,
          DanhMuc,
          CuaHang,
          GiaNhap,
          GiaBan,
          NhaCungCap,
          SoLuong,
        },
      },
      { new: true },
      function (err, samphams) {
        if (!err) {
          res.redirect("/chucuahang/cahaisan2");
        } else {
          console.log("Error during record update : " + err);
        }
      }
    );
  }