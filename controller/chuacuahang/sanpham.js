const CreateSanPham = require("../../models/Schema_Sanpham")
module.exports =  function (req, res) {
    let accountId = req.cookies.accountId;
    if (req.cookies.email) {
      CreateSanPham.find({ account: accountId }).exec(function (err, samphams) {
        res.render("views2/pages/sanpham", {
          sanps: samphams,
        });
      });
    } else res.redirect("/login");
  }