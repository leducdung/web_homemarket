
const multer = require("multer");
const CreateSanPham = require("../../models/Schema_Sanpham")  
  let diskStorage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, "public/uploads");
    },
    filename: (req, file, callback) => {
      let math = ["image/png", "image/jpeg"];
      if (math.indexOf(file.mimetype) === -1) {
        let errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
        return callback(errorMess, null);
      }
      let filename = `${Date.now()}-anh-${file.originalname}`;
      callback(null, filename);
    },
  });
  let uploadFile = multer({ storage: diskStorage }).single("file");
  module.exports = (req, res) => {
    uploadFile(req, res, (error) => {
      if (error) {
        return res.send(`Error when trying to upload: ${error}`);
      }
      const {
        TenHang,
        MaSP,
        DanhMuc,
        CuaHang,
        GiaNhap,
        GiaBan,
        NhaCungCap,
        SoLuong,
      } = req.body;
      let accountId = req.session.accountId;
      CreateSanPham.create(
        {
          linkimage: `/uploads/${req.file.filename}`,
          TenHang,
          MaSP,
          DanhMuc,
          CuaHang,
          GiaNhap,
          GiaBan,
          NhaCungCap,
          SoLuong,
          account: accountId,
        },
        function (err) {
          if (err) {
            console.log(err);
          }
        }
      );
      res.redirect("/chucuahang/cahaisan2");
    });
  }