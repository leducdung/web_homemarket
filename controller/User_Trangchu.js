const CreateSanPham = require("../models/Schema_Sanpham");

module.exports = function (req, res, next) {
  var page;
  var perpage = 16;
  if (req.query.page) {
    page = parseInt(req.query.page);
  } else {
    page = 1;
  }
  var start = (page - 1) * perpage;
  var end = start + perpage;

  sess = req.session;
  quyentruycap = sess.quyentruycap;

  var cookie = req.cookies.email;
  CreateSanPham.find({DanhMuc: "Thịt bò, Thịt heo"}).exec(function (err, samphams) {
    if (cookie) {
      res.render("pages/index", {
        msg: "uploads",
        quyentruycap,
        sanps: samphams.slice(start, end),
        pages: samphams,    
        hangs: req.session.giohang,    
      }); 
    } else res.redirect("/login");
  });
};
