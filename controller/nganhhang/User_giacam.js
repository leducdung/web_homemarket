const CreateSanPham = require("../../models/Schema_Sanpham");
module.exports = function (req, res, next) {
  var page;
  var perpage = 8;
  if (req.query.page) {
    page = parseInt(req.query.page);
  } else {
    page = 1;
  }
  var start = (page - 1) * perpage;
  var end = start + perpage;

  quyentruycap = sess.quyentruycap;
  CreateSanPham.find({DanhMuc:"Gia cầm"}).exec(function (err, samphams) {
    if (req.cookies.email) {
      res.render("pages/giacam", {
        msg: "uploads",        
        quyentruycap,
        sanps :samphams.slice(start,end),
        pages:samphams,
        hangs: req.session.giohang,  
      });
    } else res.redirect("/login");
  });
};
