const CreateSanPham = require("../../models/Schema_Sanpham");
module.exports = function (req, res, next) {
  var page;
  var perpage = 8;
  if (req.query.page) {
    page = parseInt(req.query.page);
  } else {
    page = 1;
  }
  var start = (page - 1) * perpage;
  var end = start + perpage;

  sess = req.session;
  quyentruycap = sess.quyentruycap;
  CreateSanPham.find({DanhMuc:"Rau củ quả"}).exec(function (err, samphams) {
    if (req.cookies.email) {
      res.render("pages/raucuqua", {
        msg: "uploads",
        quyentruycap,
        sanps: samphams.slice(start, end),
        pages: samphams,
        hangs: req.session.giohang,  
      });
    } else res.redirect("/login");
  });
};
