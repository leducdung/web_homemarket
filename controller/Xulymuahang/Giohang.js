const Createcard = require("../../models/Schema_Giohang")
module.exports = function (req, res) {
    const accountId   = req.cookies.accountId;
    Createcard.find({ account: accountId }).exec(function (err, soluongs) {
      res.render("partials/giohangs/giohang", {
        hangs: soluongs,
      });
    });
  }