const PostModel = require("../../models/Schema_Giohang")
module.exports = function (req, res, next) {
    PostModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      {
        $set: {
          soluong: req.body.soluong,
        },
      },
      { new: true },
      function (err, soluongs) {
        if (!err) {
          res.redirect("/giohang");
        } else {
          console.log("Error during record update : " + err);
        }
      }
    );
  }