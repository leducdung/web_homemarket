const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require("express-session");
var cookieParser = require('cookie-parser');
const Schema = mongoose.Schema;
const multer = require("multer");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const GoLogin = require("../controller/Xulydangnhap/User_Gologin");
const Login = require("../controller/Xulydangnhap/User_Login");
const GoRegister = require("../controller/Xulydangnhap/User_Dangky");
const Logout = require("../controller/Xulydangnhap/User_Logout");
const TrangChu = require("../controller/User_Trangchu");
const Cahaisan = require("../controller/nganhhang/User_Cahaisan");
const Raucuqua = require("../controller/nganhhang/User_Raucuqua");
const Giacam = require("../controller/nganhhang/User_giacam");
const Xemthem = require("../controller/Xulymuahang/Xemthem");
const Themvaogiohang = require("../controller/Xulymuahang/Themvaogiohang");
const Giohang = require("../controller/Xulymuahang/Giohang");
const Giohang1 = require("../controller/Xulymuahang/Giohang1");
const XoaHang = require("../controller/Xulymuahang/Xoahang");
const Chinhsua_Get = require("../controller/Xulymuahang/Chinhsua_get");
const Chinhsua_Post = require("../controller/Xulymuahang/Chinhsua_Post");
const Danghang = require("../controller/chuacuahang/Danghang");
const cahaisan2 = require("../controller/chuacuahang/cahaisan2");
const SanPham = require("../controller/chuacuahang/sanpham");
const Chinhsuasp = require("../controller/chuacuahang/Chinhsuasp");
const Chinhsuasp2 = require("../controller/chuacuahang/Chinhsuasp2");
const Capquyenchucuahang = require("../controller/chuacuahang/Capquyenchucuahang");
const model_Schema = require("./Schema");

var sess;

router.use(
  session({
    secret: "doraemon",
    resave: true,
    saveUninitialized: true,
    cookie: {
      maxAge: 1000 * 60 * 100000,
    },
  })
);

router.use(bodyParser());
router.use(cookieParser());

router.get("/trangchu", TrangChu);

router.get("/login", Login);

router.post("/registers", GoRegister);

router.post("/gologin", GoLogin);

router.post("/logout", Logout);

router.get("/register", function (req, res, next) {
  res.render("pages/register");
});

router.get("/gioithieu", function (req, res, next) {
  res.render("pages/gioithieu",{
    hangs: req.session.giohang,  
  });
});

router.get("/khuyenmai", function (req, res, next) {
  res.render("pages/khuyenmai",{
    hangs: req.session.giohang,  
  });
});

router.get("/cahaisan", Cahaisan);

router.get("/raucuqua", Raucuqua);

router.get("/giacam", Giacam);

router.get("/xemthem/:id", Xemthem);

router.post("/themvaogiohang", Themvaogiohang);

router.get("/buasang", function (req, res, next) {
  res.render("partials/card/buasang", {
    hangs: req.session.giohang,  
  });
});

router.get("/giohang", Giohang);
router.get("/giohang1", Giohang1);

router.get("/detele/:id", XoaHang);

router.get("/edit/:id", Chinhsua_Get);

router.post("/editt/:id", Chinhsua_Post);

router.get("/dangkychucuahang", function (req, res, next) {
  res.render("caidat/dangkychucuahang",{
    hangs: req.session.giohang,  
  });
});

router.get("/dangkydiachicuahang", function (req, res, next) {
  res.render("caidat/dangkydiachicuahang",{
    hangs: req.session.giohang,  
  });
});

router.post("/capquyenchucuahang", Capquyenchucuahang);

router.get("/chucuahang/themsanpham", function (req, res) {
  res.render("views2/pages/themsanpham");
});

router.post("/chucuahang/upload2", Danghang);

router.get("/chucuahang/cahaisan2", cahaisan2);

router.get("/chucuahang/about", function (req, res) {
  res.render("views2/pages/about");
});

router.get("/chucuahang/hoadonbanle", function (req, res) {
  res.render("views2/pages/hoadonbanle");
});

router.get("/chucuahang/hoadontrahang", function (req, res) {
  res.render("views2/pages/hoadontrahang");
});

router.get("/chucuahang/khachhang", function (req, res) {
  res.render("views2/pages/khachhang");
});

router.get("/chucuahang/khuyenmai", function (req, res) {
  res.render("views2/pages/khuyenmai");
});

router.get("/chucuahang/lichsuxoakho", function (req, res) {
  res.render("views2/pages/lichsuxoakho");
});

router.get("/chucuahang/lichsuxoasp", function (req, res) {
  res.render("views2/pages/lichsuxoasp");
});

router.get("/chucuahang/phieunhapkho", function (req, res) {
  res.render("views2/pages/phieunhapkho");
});

router.get("/chucuahang/phieuxuatkho", function (req, res) {
  res.render("views2/pages/phieuxuatkho");
});

router.get("/chucuahang/sanpham", SanPham);

router.get("/chucuahang/chinhsuasp/:id", Chinhsuasp);

router.post("/chucuahang/chinhsuasp2/:id", Chinhsuasp2);

router.get("/chucuahang/suagiaban", function (req, res) {
  res.render("views2/pages/suagiaban");
});

router.get("/chucuahang/suagianhap", function (req, res) {
  res.render("views2/pages/suagianhap");
});

module.exports = router;
