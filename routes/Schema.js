
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userSchema = new Schema({
    name: String,
    email: String,
    password: String,
    quyentruycap: String
  });
  
  // bang hang
  
  const cardSchema = new  Schema({
    soluong: Number,
    tenhang: String,
    tygia: Number,
    tinhtrang: String,
    account: {
      type: mongoose.Types.ObjectId,
      required: true
    }
  });
  
  const sanphamSchema = new  Schema(
    {
      linkimage: String,
      TenHang: String,
      MaSP: String,
      DanhMuc: String,
      CuaHang: String,
      GiaNhap: Number,
      GiaBan: Number,
      NhaCungCap: String,
      SoLuong: Number,
      account :{
        type :mongoose.Types.ObjectId,
         
      }
    },
    
  );
  module.exports = {
    userSchema : userSchema,
    cardSchema: cardSchema,
    sanphamSchema : sanphamSchema
  }