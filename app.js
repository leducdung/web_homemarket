var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
const express = require('express');
var bodyParser = require('body-parser');

const app = express();
const port = 3000;

var indexRouter = require('./routes/index');
var chucuahangRouter = require("./routes/chucuahang");

// mongoss
var db = mongoose.connection;
mongoose.connect('mongodb://localhost:27017/account', { useNewUrlParser: true });

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('Connected to db');
});

mongoose.connection.on('error', err => {
  logError(err);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true  }));
app.use(bodyParser.json());

app.use("/chucuahang", chucuahangRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  req.db = db;
  res.locals.session = req.session;
  next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
 
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

